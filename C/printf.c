#include <stdio.h>

int main()
{
	int five = 5, six = 6, three = 3;
	int result = (five * six) / three;
	printf("The result of %d multiplied by %d is actually %d", five, six, result);
	return 0;

}
